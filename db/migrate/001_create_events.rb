class CreateEvents < ActiveRecord::Migration
  def self.up
    create_table :events do |t|
    t.column :name, :string
    t.column :start, :integer
    t.column :finish, :integer
    t.column :day, :integer
    t.column :color, :string
    t.timestamps
    end
  end

  def self.down
    drop_table :events
  end
end
