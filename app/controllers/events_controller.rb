class EventsController < ApplicationController
  def index
    list
    render :action => 'list'
  end

  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
#  verify :method => :post, :only => [ :destroy, :create, :update ],
  #       :redirect_to => { :action => :list }

  def list
    @events = :events
  end

  def show
    @event = Event.find(params[:id])
  end

  def showall
    @events = Event.find(:all)
  end

  def new
    @event = Event.new
  end

  def create
    @event = Event.new(params[:event])
    if @event.save
      flash[:notice] = 'Event was successfully created.'
      redirect_to :action => 'list'
    else
      render :action => 'new'
    end
  end

  def edit
    @event = Event.find(params[:id])
  end

  def chart
    @event = Event.find(:all)
    render :layout => false
  end
 def chartsm
    @event = Event.find(:all)
    render :layout => false
  end


  def update
    @event = Event.find(params[:id])
    if @event.update_attributes(params[:event])
      flash[:notice] = 'Event was successfully updated.'
      redirect_to :action => 'show', :id => @event
    else
      render :action => 'edit'
    end
  end

  def destroy
    begin
    Event.find(params[:id]).destroy
    rescue ActiveRecord::RecordNotFound
      flash[:notice] = 'Invalid event id.'
    end
    redirect_to :action => 'showall'
  end

end
